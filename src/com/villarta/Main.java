package com.villarta;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("What is your firstName? ");
        String firstName = scanner.nextLine();
        System.out.println("What is your lastName? ");
        String lastName = scanner.nextLine();
        System.out.println("What is your grade in English? ");
        double englishGrade = scanner.nextInt();
        System.out.println("What is your grade in Mathematics? ");
        double mathGrade = scanner.nextInt();
        System.out.println("What is your grade in Science? ");
        double scienceGrade = scanner.nextInt();
        double average = (englishGrade + mathGrade + scienceGrade) / 3;
        System.out.println("Hi, " + firstName + " " + lastName + "! Your average is " + average);

    }
}
